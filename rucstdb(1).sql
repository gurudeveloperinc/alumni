-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2017 at 02:10 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rucstdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `evid` int(255) NOT NULL DEFAULT '0',
  `uid` int(255) NOT NULL,
  `evtittle` varchar(1000) DEFAULT NULL,
  `evdate` varchar(255) DEFAULT NULL,
  `evdetails` varchar(5000) NOT NULL,
  `evcomment` varchar(1000) NOT NULL,
  `venue` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`evid`, `uid`, `evtittle`, `evdate`, `evdetails`, `evcomment`, `venue`, `created_at`, `updated_at`) VALUES
(0, 2, 'AlumniHomecoming', 'july-27-2017', 'Thanks given', 'Come ye All', 'Mc Hill', '2017-02-28 03:58:02', '2017-02-28 03:58:02');

-- --------------------------------------------------------

--
-- Table structure for table `gallary`
--

CREATE TABLE IF NOT EXISTS `gallary` (
  `gid` int(255) NOT NULL,
  `uid` int(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `caption` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallary`
--

INSERT INTO `gallary` (`gid`, `uid`, `url`, `caption`, `created_at`, `updated_at`) VALUES
(1, 1, 'http://url.com', 'fsdlfkj', '2017-02-22 07:36:15', '2017-02-22 07:36:15'),
(2, 2, 'http://love', 'love', '2017-02-22 15:27:54', '2017-02-22 15:27:54'),
(3, 2, 'hkjkl''l', 'ghkljk;''p', '2017-02-26 07:35:08', '2017-02-26 07:35:08'),
(4, 2, 'http://127.0.0.1:8000/uploads/fobes.PNG', 'Car', '2017-03-02 04:29:50', '2017-03-02 04:29:50'),
(5, 2, 'http://127.0.0.1:8000/uploads/contor.png', 'game', '2017-03-02 04:33:36', '2017-03-02 04:33:36'),
(6, 2, 'http://127.0.0.1:8000/uploads/timetable.PNG', 'james', '2017-03-02 04:34:37', '2017-03-02 04:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `nid` int(255) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `details` varchar(1000) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`nid`, `uid`, `title`, `location`, `details`, `comment`, `created_at`, `updated_at`) VALUES
(0, 2, 'My news', 'Nima Alaska', 'what man', 'go home', '2017-02-23 06:28:35', '2017-02-23 06:28:35');

-- --------------------------------------------------------

--
-- Table structure for table `upevent`
--

CREATE TABLE IF NOT EXISTS `upevent` (
  `upevid` int(255) NOT NULL DEFAULT '0',
  `uid` int(255) NOT NULL,
  `evdate` datetime NOT NULL,
  `caption` varchar(255) NOT NULL,
  `upevtittle` varchar(255) NOT NULL,
  `speaker` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `upid` int(10) NOT NULL,
  `uid` int(255) NOT NULL,
  `reporter` varchar(255) NOT NULL,
  `tittle` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `comment` varchar(1000) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`upid`, `uid`, `reporter`, `tittle`, `url`, `comment`, `created_at`, `updated_at`) VALUES
(1, 2, 'Raj', 'MatLab And Simulink', 'http://127.0.0.1:8000/uploads/literatura-matlab.pdf', 'Good Beginner tutorial', '2017-03-02 06:05:06', '2017-03-02 06:05:06'),
(2, 2, 'Prof KK Prah', 'Multi-party Democracy in Africa', 'http://127.0.0.1:8000/uploads/Prof KK Prah - Multi-Party Democracy and It’s Relevance in Africa (1).pdf', 'Recommendable', '2017-03-02 06:37:19', '2017-03-02 06:37:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `url`, `bio`, `country`) VALUES
(1, 'Bilal', 'bilal@regent.edu.gh', '$2y$10$Aon0.5VPAWSPmnAvA2cn4.khmhBquh7/ECj40HKM1UvpFFbfmJETq', NULL, '2017-02-22 07:26:24', '2017-02-22 07:26:24', NULL, 'HR at self Employed', 'Ghana'),
(2, 'Bilal', 'bilallsuraj@gmail.com', '$2y$10$OuY3lpwqx.qFa4p/4f99c.POwMFjfjO7oB4crhZv5RvasKlhPHTQW', '6rMVrZXFi3HY7DW0BKEdHV65az8xMWyF1wkmzQJMtT1fcFhhERU3IkqE1VNs', '2017-02-22 14:08:40', '2017-02-22 14:08:40', NULL, 'CIO at code self Technologies', 'Nigeria');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`evid`);

--
-- Indexes for table `gallary`
--
ALTER TABLE `gallary`
  ADD PRIMARY KEY (`gid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`nid`);

--
-- Indexes for table `upevent`
--
ALTER TABLE `upevent`
  ADD PRIMARY KEY (`upevid`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`upid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gallary`
--
ALTER TABLE `gallary`
  MODIFY `gid` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `upid` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
