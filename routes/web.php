<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ReadController@index');
Route::get('/view-resources','ReadController@getViewResources');
Route::get('/view-news','ReadController@getNews');
Route::get('/view-events','ReadController@getEvent');
Route::get('/gallary','ReadController@getGallary');
Route::get('/profile','ReadController@getProfile');
Route::get('/manage','HomeController@manageAlumni');
Route::get('/reports','HomeController@viewReports');
Route::get('/apply/{jid}','HomeController@applyForJob');

Route::get('/featured/1','HomeController@featuredOne');
Route::get('/featured/2','HomeController@featuredTwo');
Route::get('/featured/3','HomeController@featuredThree');

Route::post('/add-comment','HomeController@postAddComment');
Route::get('/view-alumni/{year}','HomeController@viewAlumni');

Route::get('/create-gallery','HomeController@getCreateGallery');
Route::post('/create-gallery','HomeController@postCreateGallery');

Route::get('/add-news','HomeController@getCreateNews');
Route::post('/create-news','HomeController@postCreateNews');

Route::get('/add-event','HomeController@getCreateEvent');
Route::post('/create-event','HomeController@postCreateEvent');

Route::get('/add-resource','HomeController@getAddResource');
Route::post('/add-resource','HomeController@postCreateUpload');

Route::get('/add-blog','HomeController@getAddBlog');
Route::post('/add-blog','HomeController@postAddBlog');
Route::get('/view-blog','HomeController@viewBlog');

Route::get('/view-jobs','HomeController@viewJobs');
Route::get('/add-job','HomeController@getAddJob');
Route::post('/add-job','HomeController@postAddJob');


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/logout','HomeController@logout');
