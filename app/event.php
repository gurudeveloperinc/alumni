<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    protected $primaryKey = 'evid';
    public $table = 'event';
}
