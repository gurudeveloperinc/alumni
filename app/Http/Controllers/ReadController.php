<?php

namespace App\Http\Controllers;

use App\gallary;
use App\gallery;
use App\news;
use App\event;
use App\User;
use App\upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReadController extends Controller
{



    public function index()
    {
        $users = User::all();
	    $news = news::all()->sortByDesc("created_at")->take(3);
	    $events = event::all()->sortByDesc("created_at")->take(3);
	    $resources = upload::all()->sortByDesc('created_at')->take(3);

        return view('welcome',
            [
                'users' => $users,
	            'news' => $news,
	            'events' => $events,
	            'resources' => $resources
            ]);
    }

    public function getGallary()
    {
        $gallary = gallery::all()->sortByDesc('created_at'); // newest comes first
        $news = news::all()->last();
        $event = event::all()->last();
        return view('gallary',
            [
                'gallary' => $gallary,
                'news' => $news,
                'event' => $event
            ]);


    }

    public function getProfile()
    {
        return view('profile');
    }

    public function getNews()
    {
        $news = news::all()->sortByDesc('created_at');
        $event = event::all()->last();
        return view('news',[
            'news' => $news,
            'event' => $event,


        ]);
    }



    public function getEvent()
    {
        $events = event::all()->sortByDesc('created_at'); // newest comes first
        $news = news::all()->last();
        $event = event::all()->last();
        return view('event',[
            'events' => $events,
            'event' => $event,
            'news' => $news
        ]);

    }

    public function  getViewResources()
    {
        $uploads = upload::all()->sortByDesc('created_at'); // newest comes first
        $news = news::all()->last();
        $event = event::all()->last();
        return view('viewResources',[
            'uploads' => $uploads,
            'news' => $news,
            'event' => $event
        ]);

    }



}
