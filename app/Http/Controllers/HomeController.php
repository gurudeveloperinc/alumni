<?php

namespace App\Http\Controllers;

use App\blog;
use App\comment;
use App\event;
use App\gallery;
use App\job;
use App\news;
use App\events;
use App\User;
use App\upload;
use App\uploads;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $users = User::all();
        $news = news::all()->sortByDesc("created_at");
	    $uploads = upload::all();
        $event = event::all()->last();
        return view('home',
            [
                'users' => $users,
                'news' => $news,
                'event' => $event,
	            'uploads' => $uploads
            ]);
    }

    public function getCreateGallery()
    {
        return view('createnews');
    }

    public function postCreateGallery(Request $request)
    {
        $gallery = new gallery();
        $gallery->caption = $request->input('caption');

        if($request->hasFile("url")){
            $photoName = $request->file("url")->getClientOriginalName();
            $request->file("url")->move('uploads',$photoName);
            $gallery->url = url('uploads/'.$photoName);
        }
        else
            $gallery->url = url('default.jpg');

            $gallery->uid = Auth::user()->uid;
            $status =  $gallery->save();

        if($status)
        $request->session()->flash('success', "Gallery created successfully");
        else
            $request->session()->flash('error',"Something went wrong please try again.");

        return redirect('/create-gallery');
    }

    public function getCreateNews()
    {
        return view('createnews');
    }

    public function postCreateNews(Request $request)
    {
        $news = new news();
        $news->title = $request->input('title');
        $news->location = $request->input('location');
        $news->uid = Auth::user()->uid;
        $news->details = $request->input('details');

        $status = $news->save();

	    if($status) $request->session()->flash("success", "Successfully Added News");
        else $request->session()->flash("error", "Sorry an error occurred");

	    return redirect('/view-news');
    }

	public function postAddComment( Request $request ) {

		$comment = new comment();
		$comment->comment = $request->input('comment');
		$comment->bid = $request->input('bid');
		$comment->uid = Auth::user()->uid;
		$status = $comment->save();


		if($status)	$request->session()->flash("success","Comment added successfully");
		else $request->session()->flash("error", "Sorry an error occurred");


		return redirect('/view-blog');
	}

    public function getCreateEvent()
    {
        return view('eventcreate');
    }

	public function manageAlumni() {
		$users = User::all();
		return view('manageAlumni', ['users' => $users]);
	}

	public function viewAlumni( $year ) {
		if($year == "All"){
			$users = User::all();
		}else{
			$users = User::where('year',$year)->get();
		}

		return view('viewAlumni',[
			'users' =>$users,
			'year' => $year
		]);
	}

	public function featuredOne() {
		return view('featuredOne');
	}

	public function featuredTwo() {
		return view('featuredTwo');
	}

	public function featuredThree() {
		return view('featuredThree');
	}

	public function getAddJob() {
		return view('addJob');
	}

	public function viewJobs() {
		$jobs = job::all();
		return view('viewJobs',['jobs' => $jobs]);
	}

    public function postCreateEvent(Request $request)
    {
        $event = new event();
        $event->evtittle = $request->input('evtittle');
	    $event->evyear = $request->input('evyear');
        $event->evdetails = $request->input('evdetails');
        $event->uid = Auth::user()->uid;
        $event->evcomment = $request->input('evcomment');
        $event->evdate = $request->input('evdate');
        $event->venue = $request->input('evvenue');
        if($request->hasFile("url")){
            $docummentName = $request->file("url")->getClientOriginalName();
            $request->file("url")->move('event',$docummentName);
            $event->url = url('event/'.$docummentName);
        }
        else
            $event->url = url('default.jpg');

        $event->uid = Auth::user()->uid;
        $status =  $event->save();

        if($status)
            $request->session()->flash('success', "Documment Uploaded successfully");
        else
            $request->session()->flash('error',"Something went wrong please try again.");
        return redirect('/create-event');
    }

	public function viewReports() {

		$year = "";
		if(Input::has('year')){
			$year = Input::get('year');
		} else {
			$year = 'All';

		}


		$days = "";
		if(Input::has('days')){
			$days = Input::get('days');
		} else {
			$days = 'All';

		}


		if($year == "All"){
			$events = event::all();
		} else {

			$events = event::all()->where('evyear',$year);
		}

		if($days == "All"){
			$jobs = job::all();
		} else {
			$jobs = job::all()->where('created_at', '>', Carbon::now()->subDay($days));
		}




		return view('viewReports',[
			'events' => $events,
			'year' => $year,
			'jobs' => $jobs,
			'days' => $days
		]);
	}


	public function applyForJob( Request $request, $jid ) {

		$job = job::find($jid);
		$cv =Auth::user()->cv;

		if($cv != null){

			mail($job->email,"Job application for $job->title", "Good day,\nplease find below a link to my CV as an application for the above position. \n\n $cv ");
		}

		$request->session()->flash('success','Application successful');

		return redirect('/view-jobs');
	}

    public function getAddResource()
    {
        return view('createupload');
    }

    public function postCreateUpload(Request $request)
    {
        $upload = new upload();
        $upload->title = $request->input('tittle');
        $upload->reporter = $request->input('reporter');
        $upload->uid = Auth::user()->uid;
        $upload->comment = $request->input('comment');

	    if($request->hasFile("image")){
		    $docummentName = $request->file("image")->getClientOriginalName();
		    $request->file("url")->move('uploads',$docummentName);
		    $upload->image = url('uploads/'.$docummentName);
	    }
	    else
		    $upload->image = url('images/default.jpg');


        if($request->hasFile("url")){
            $docummentName = $request->file("url")->getClientOriginalName();
            $request->file("url")->move('uploads',$docummentName);
            $upload->url = url('uploads/'.$docummentName);
        }
        else
            $upload->url = url('default.jpg');

        $upload->uid = Auth::user()->uid;
        $status =  $upload->save();

        if($status)
            $request->session()->flash('success', "Document Uploaded successfully");
        else
            $request->session()->flash('error',"Something went wrong please try again.");

        return redirect('/add-resource');

    }

	public function getAddBlog() {
		return view('addBlog');
	}

	public function viewBlog() {
		$blog = blog::all()->sortByDesc("created_at");
		return view('blog',['blog' => $blog]);
	}

	public function postAddBlog( Request $request ) {
		$blog = new blog();
		$blog->title = $request->input('title');
		$blog->details = $request->input('details');
		$blog->uid = Auth::user()->uid;
		$blog->save();

		$request->session()->flash("success","Blog post added");
		return redirect('/add-blog');
	}

	public function postAddJob( Request $request ) {

		$job = new job();
		$job->title = $request->input('title');
		$job->description = $request->input('description');
		$job->email = $request->input('email');
		$job->expiry = $request->input('expiry');
		$job->uid = Auth::user()->uid;
		$status = $job->save();

		if($status)
			$request->session()->flash("success","Job added");
		else
			$request->session()->flash("error","Sorry an error occurred");

		return redirect('/add-job');

	}


	public function logout() {
		Auth::logout();
		return redirect('/');
	}



}
