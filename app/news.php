<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class news extends Model
{
    //
    protected $primaryKey = 'nid';
    public $table = 'news';
}
