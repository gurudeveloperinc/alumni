@extends('layouts.app')

@section('content')

    <div class="container">
        <label>Display Jobs</label>
        <input id="jobCheckbox" checked type="checkbox">
        <label>Display Events</label>
        <input id="eventsCheckbox" checked type="checkbox">
        <div class="panel panel-default col-md-10" id="events">
            <div class="panel-heading">
                Event Repoorts -
                {{$year}} Events
                <select id="year" style="color:black; " class="form-control right">
                    <option disabled selected>Select Year</option>
                    <option>All</option>
                    @for($i = 10; $i < 31; $i++)
                    <option>20{{$i}}</option>
                    @endfor
                </select>

                <script>
                    $(document).ready(function(){

                        var jobsCheckbox = $('#jobCheckbox');
                        var eventsCheckbox = $('#eventsCheckbox');

                        jobsCheckbox.on('change',function(){
                            $('#jobs').toggleClass('hide');
                        });

                        eventsCheckbox.on('change',function(){
                            $('#events').toggleClass('hide');
                        });

                        var year = $('#year');
                        year.on('change', function () {
                            window.location = "{{url('/reports')}}?year=" + year.val();
                        })
                    })
                </script>

            </div>

            <div class="panel-body">
                <table class="table table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Details</th>
                        <th>Venue</th>
                        <th>Created On</th>

                    </tr>

                    @foreach($events as $item)
                        <tr>
                            <td>{{$item->evtittle}}</td>
                            <td>{{$item->evdate}}</td>
                            <td>{{$item->evdetails}}</td>
                            <td>{{$item->venue}}</td>
                            <td>{{$item->created_at}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>

        </div>

        <div class="panel panel-default col-md-10" id="jobs">
            <div class="panel-heading">
                Job Reports -
                Last {{$days}} Days Events <span style="float:right;">{{count($jobs)}} jobs in the selected duration</span>
                <select id="days" style="color:black; " class="form-control right">
                    <option disabled selected>Select Days</option>
                    <option>All</option>
                        <option value="1">Today</option>
                        <option>7</option>
                        <option>14</option>
                        <option>30</option>
                        <option>90</option>
                        <option value="365">Year</option>

                </select>

                <script>
                    $(document).ready(function(){
                        var year = $('#days');
                        year.on('change', function () {
                            window.location = "{{url('/reports')}}?days=" + year.val();
                        })
                    })
                </script>

            </div>

            <div class="panel-body">
                <table class="table table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Details</th>

                        <th>Created On</th>
                        <th></th>
                    </tr>

                    @foreach($jobs as $item)
                        <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->created_at}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>

        </div>

    </div>

@endsection

