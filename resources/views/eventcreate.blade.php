
    @extends('layouts.app')
    @section('content')
        <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">Add Event</div>
                            <div class="panel-body">
                             <form  enctype="multipart/form-data" role="form" method="POST" action="{{ url('/create-event') }}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="tittle">Title</label>
                            <input class="form-control" type="text" name="evtittle" id="evtittle" value="">

                            <label for="evdate">Event Day and Month</label>
                            <input placeholder="May 12th 2017" class="form-control" type="text" name="evdate" id="evdate" value="">

                            <label for="evdate">Event Year</label>
                            <input placeholder="EG. 2017" class="form-control" type="text" name="evyear" id="evyear" value="">


                            <label for="details">Details</label>
                            <textarea class="form-control" name="evdetails" id="evdetails"></textarea>

                            <label for="comment">Comment</label>
                            <input class="form-control" type="text" name="evcomment" id="evcomment" value="">

                            <label for="venue">Venue</label>
                            <input class="form-control" type="text" name="evvenue" id="evvenue" value="">

                            <label for="venue">Venue</label>
                            <input class="" type="file" name="evvenue" id="evvenue" >

                        </div>
                        <button type="submit" class="btn btn-primary">Add Event</button>
                    </form>
                </div>

            </div>
        </div>
                </div>
        </div>

 @endsection
