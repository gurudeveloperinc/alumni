@extends('layouts.app')

@section('content')
    <?php use \Carbon\Carbon; ?>

    @include('notification')
    @foreach($blog as $item)
    <div class="well">
        <h3>{{$item->title}}</h3>
        <p style="font-size: 20px;">{{$item->details}}</p>

        <div>
            <h3>Comments</h3>
            <ul>
                @foreach($item->Comments as $comment)
                <li>{{$comment->User->name}} <br>
                    <span style="font-size: 12px;" >{{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->diffForHumans()}}</span>
                    <br>
                    {{$comment->comment}} </li>
                @endforeach
            </ul>
            <form method="post" action="{{url('/add-comment')}}">
                {{csrf_field()}}
                <div class="form-group">
                    <textarea name="comment"></textarea>
                    <input type="hidden" name="bid" value="{{$item->bid}}">
                </div>

                    <button type="submit" class="btn btn-primary">Add</button>

            </form>
        </div>

    </div>

    @endforeach

@endsection