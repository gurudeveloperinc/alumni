@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="panel panel-default col-md-8 col-md-offset-1">
            <div class="panel-heading">
                Recent Registrations
            </div>
            <div class="panel-body">
                <table class="table table-hover table-striped table-responsive">
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Country</th>
                        <th>Registered On</th>
                    </tr>

                    @foreach($users as $item)
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->country}}</td>
                        <td>{{$item->created_at}}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="panel panel-default col-md-8 col-md-offset-1">
            <div class="panel-heading">
                Recent News
            </div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Location</th>
                        <th>Details</th>
                        <th>Published On</th>
                        <th></th>
                    </tr>

                    @foreach($news as $item)
                        <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->location}}</td>
                            <td style="width:150px; overflow: scroll">{{$item->details}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>
                                <a href="{{url('/view-news/' . $item->nid)}}" class="btn btn-primary">View</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        @if($uploads != "[]")
            <div class="panel panel-default col-md-8 col-md-offset-1">
                <div class="panel-heading">
                    Recent Resources
                </div>
                <div class="panel-body">

                    @foreach($uploads as $item)
                    <div class="col-md-3">

                        <img src="{{$item->image}}" class="img-responsive"><br>
                        <p>{{$item->title}}</p>
                        <a href="{{$item->url}}">Download</a>

                    </div>
                    @endforeach

                </div>
            </div>
        @endif
    </div>

    <div class="clearfix"></div>

@endsection