@extends('layouts.app')

@section("content")
    <div class="container">

        <div class="panel panel-default col-md-8 col-md-offset-1">
            <div align="center" class="panel-heading">
                All Resources
            </div>
            <div class="panel-body">

                @foreach($uploads as $item)
                    <div class="col-md-3">

                        <img src="{{$item->image}}" class="img-responsive"><br>
                        <p>{{$item->title}}</p>
                        <a href="{{$item->url}}">Download</a>

                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
