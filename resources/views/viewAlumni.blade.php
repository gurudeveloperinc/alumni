@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default col-md-10">
            <div class="panel-heading">
                {{$year}} Alumni
                <select id="year" style="color:black; " class="form-control right">
                    <option disabled selected>Select Year</option>
                    <option>All</option>
                    <option>2017</option>
                    <option>2016</option>
                </select>

                <script>
                    $(document).ready(function(){
                        var year = $('#year');
                        year.on('change', function () {
                            window.location = "{{url('/view-alumni')}}/" + year.val();
                        })
                    })
                </script>

            </div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Country</th>
                        <th>Year</th>
                        <th>Registered On</th>
                        <th>Added</th>
                        @if(Auth::user()->role == "Admin")
                        <th></th>
                            @endif
                    </tr>

                    @foreach($users as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->country}}</td>
                            <td>{{$item->year}}</td>
                            <td>{{$item->bio}}</td>
                            <td>{{\Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->diffForHumans()}}</td>
                            @if(Auth::user()->role == "Admin")
                            <td>
                                <a href="{{url('/delete-user/' . $item->uid)}}" class="btn btn-danger">Delete</a>
                            </td>
                                @endif
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection

