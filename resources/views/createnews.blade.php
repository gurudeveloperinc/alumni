@extends('layouts.app')
@section('content')
    <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">News</div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/create-news') }}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="tittle" class="col-md-4 control-label">Title</label>

                                    <div class="col-md-6">
                                        <input id="tittle" type="text" class="form-control" name="title" value="">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="location" class="col-md-4 control-label">Location</label>

                                    <div class="col-md-6">
                                        <input id="location" type="text" class="form-control" name="location">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Details" class="col-md-4 control-label">Details</label>

                                    <div class="col-md-6">
                                        <textarea id="details" class="form-control" name="details" ></textarea>

                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>


@endsection