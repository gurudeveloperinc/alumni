

<!DOCTYPE html>
<html>
<head>

    <title>REGENT ALUMNI SYSTEM - RUCST</title>

    <!-- For-Mobile-Apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Regent university School of Informatics, Engineering and Technology" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //For-Mobile-Apps -->

    <!-- Bootstrap-Core-CSS --> <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom-Theme-Styling --> <link href="css/newstyle.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Owl-Carousel-Styling --> <link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all">
    <!-- favicon -->  <link href="favicon.ico" type="image/x-icon" rel="icon">

    <!-- Web-Fonts -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <!-- //Web-Fonts -->

    <!-- Supportive-JavaScript --><script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Necessary-JS-File-For-Bootstrap --><script type="text/javascript" src="js/bootstrap.min.js"></script>


    <script src="js/main.js"></script>
    <script src="js/modernizr.custom.js"></script>

</head>
<body>
<div class="container">

    <!-- Header-Starts-Here -->

    <!-- Navigation -->
    <nav class="navbar navbar-inverse">

        <img src="images/newlogo.PNG" style="margin-bottom: 5px;">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="social-icons">
            <ul class="social slideanim" id="follow">
                <li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
                <li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
                <li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
                <li><a href="#" class="instagram" title="Go to Our Instagram Account"></a></li>
                <li><a href="#" class="youtube" title="Go to Our Youtube Channel"></a></li>
            </ul>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
            <ul class="nav navbar-nav" style="padding-left:220px;">
                <li class="cl-effect-7 homeli"><a href="{{url('/')}}">Home</a></li>
                <li class="cl-effect-7 aboutli "><a href="{{url('/view-news')}}">News</a></li>
                <li class="cl-effect-7 coursesli"><a href="{{url('/view-events')}}">Events</a></li>
                <li class="cl-effect-7 lecturersli"><a href="#contact">Contact</a></li>
                @if(Auth::guest())
                   <li class="cl-effect-7 lecturersli"><a href="{{url('/login')}}">Login</a></li>
                @else
                    <li class="cl-effect-7 lecturersli"><a href="{{url('/view-blog')}}">Blog</a></li>
                    <li class="cl-effect-7 lecturersli"><a href="{{url('/home')}}">Dashboard</a></li>
                @endif

            </ul>
        </div>
        <!-- //Navbar-collapse -->
    </nav>

@yield('content')

        <!-- Footer-Starts-Here -->
    <div class="footer">

        <div class="footer-info slideanim">

            <div class="col-md-4  col-md-offset-2 footer-info-grid address">
                <h4>ADDRESS</h4>
                <address>
                    <ul>
                        <li>Regent University</li>
                        <li>McCarthy hill</li>
                        <li>Weija, Accra</li>
                        <li>Telephone : +233893839487</li>
                        <li>Email : <a class="mail" href="mailto:info@regent.edu.gh">info@regent.edu.gh</a></li>
                    </ul>
                </address>
            </div>
            <div class="col-md-4 col-md-offset-1 footer-info-grid email">
                <h4>NEWSLETTER</h4>
                <p>
                    Subscribe to our newsletter and we will
                    inform you about our events and green book activities.
                </p>

                <form class="newsletter">
                    <input class="email" type="email" placeholder="Your email...">
                    <input type="submit" class="submit" value="">
                </form>
            </div>
            <div class="clearfix"></div>
        </div>


        <div class="copyright slideanim">
            <p>&copy; 2016 RUCST. All Rights Reserved | Created by <a href="#"> Level 300 ISS Evening Students</a></p>
        </div>


    </div>
    <!-- //Footer-Ends-Here -->

</div>

<!-- SmoothScroll-JavaScript -->
<script>
    $(document).ready(function(){
        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar li a.scroll, .footer a[href='#myPage']").on('click', function(event) {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 900, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        });

        $(window).scroll(function() {
            $(".slideanim").each(function(){
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    })
</script>
<!-- //SmoothScroll-JavaScript -->

<!-- Slideanim-JavaScript -->
<script type="text/javascript">
    $(window).scroll(function() {
        $(".slideanim").each(function(){
            var pos = $(this).offset().top;

            var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
                $(this).addClass("slide");
            }
        });
    });
</script>
<!-- //Slideanim-JavaScript -->

<!-- Owl-Carousel-JavaScript -->
<script src="js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        $("#owl-demo").owlCarousel ({
            items : 4,
            lazyLoad : true,
            autoPlay : true,
            pagination : false,
        });
    });
</script>
<!-- //Owl-Carousel-JavaScript -->

<!-- Banner-Slider-JavaScript -->
<script src="js/responsiveslides.min.js"></script>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            nav: true,
            speed: 500,
            namespace: "callbacks",
            pager: true,
        });
    });
</script>
<!-- //Banner-Slider-JavaScript -->

<!-- Slide-To-Top JavaScript (No-Need-To-Change) -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */
        $().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //Slide-To-Top JavaScript -->

<!-- Smooth-Scrolling-JavaScript -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- //Smooth-Scrolling-JavaScript -->

<!-- FlexSlider-JavaScript -->
<script defer src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
    $(function(){
        SyntaxHighlighter.all();
    });
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>
<!-- //FlexSlider-JavaScript -->

</body>
</html>