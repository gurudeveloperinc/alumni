<!DOCTYPE HTML>
<html>
<head>
<title>Regent Alumni Portal</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="{{url('css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="{{url('css/style.css')}}" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="{{url('css/font-awesome.css')}}" rel="stylesheet">
<!-- jQuery -->
<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="{{url('css/icon-font.min.css')}}" type='text/css' />
<!-- //lined-icons -->
<script src="{{url('js/jquery-1.10.2.min.js')}}"></script>
<script src="js/amcharts.js"></script>
<script src="js/serial.js"></script>
<script src="js/light.js"></script>
<script src="js/radar.js"></script>
<link href="css/barChart.css" rel='stylesheet' type='text/css' />
<link href="css/fabochart.css" rel='stylesheet' type='text/css' />
<!--clock init-->
<script src="js/css3clock.js"></script>
<!--Easy Pie Chart-->
<!--skycons-icons-->
<script src="js/skycons.js"></script>

<script src="js/jquery.easydropdown.js"></script>

<!--//skycons-icons-->
</head>
<body>
   <div class="page-container">
   <!--/content-inner-->
	<div class="left-content">
	   <div class="inner-content">
		<!-- header-starts -->
			<div class="header-section">
						<!--menu-right-->
						<div class="top_menu">

							<!--/profile_details-->
								<div class="profile_details_left">

							</div>
							<div class="clearfix"></div>
							<!--//profile_details-->
						</div>
						<!--//menu-right-->
					<div class="clearfix"></div>
				</div>
					<!-- //header-ends -->
						<div class="outter-wp">

                            @yield('content')

										<!--//outer-wp-->
									</div>
									 <!--footer section start-->
										<footer>
										   <p>&copy 2017 Regent University . All Rights Reserved </p>
										</footer>
									<!--footer section end-->
								</div>
							</div>
				<!--//content-inner-->
			<!--/sidebar-menu-->
				<div class="sidebar-menu">

					<header class="logo">
					<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="{{url('/')}}"> <span id="logo"> <h1>Regent Alumni Portal</h1></span>
				  </a>
				</header>
			<div style="border-top:1px solid rgba(69, 74, 84, 0.7)"></div>
			<!--/down-->
                    @if(!Auth::guest())
							<div class="down">
									  <a href="{{url('/')}}"><img src="{{Auth::user()->photo ? :  url('/images/logo.png')}}"></a>
									  <a href="{{url('/')}}"><span class=" name-caret">

                                                {{Auth::user()->name}}

                                          </span></a>
									 <p>

                                         {{Auth::user()->bio}} <br>
                                         <span style="color:#025D2E">{{Auth::user()->role}}</span>

                                     </p>
									<ul>
									<li><a class="tooltips" href="{{url('/profile')}}"><span>Profile</span><i class="lnr lnr-user"></i></a></li>
										<li><a class="tooltips" href="{{url('/')}}"><span>Home</span><i class="lnr lnr-home"></i></a></li>
										<li><a class="tooltips" href="{{url('/logout')}}"><span>Log out</span><i class="lnr lnr-power-switch"></i></a></li>
										</ul>
									</div>

							   <!--//down-->
                           <div class="menu">
									<ul id="menu" >
										<li><a href="{{url('/home')}}"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
										 <li ><a href="#"><i class="fa fa-table"></i> <span> News</span> <span class="fa fa-angle-right" style="float: right"></span></a>
										   <ul>
                                               @if(Auth::user()->role == "Admin")
											<li  ><a href="{{url('/add-news')}}">Add News</a></li>
                                               @endif
                                           <li  ><a href="{{url('/view-news')}}">View News</a></li>

										  </ul>
										</li>
                                        <li ><a href="{{url('/view-events')}}"><i class="fa fa-table"></i> <span> Events</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                                            <ul>
                                                @if(Auth::user()->role == "Admin")
                                                <li  ><a href="{{url('/add-event')}}">Add Event</a></li>
                                                @endif
                                                <li  ><a href="{{url('/view-events')}}">View Events</a></li>

                                            </ul>
                                        </li>
                                        <li ><a href="#"><i class="fa fa-table"></i> <span> Resources</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                                            <ul>
                                                <li  ><a href="{{url('/add-resource')}}">Add Resource</a></li>
                                                <li  ><a href="{{url('/view-resources')}}">View Resources</a></li>

                                            </ul>
                                        </li>
                                        <li ><a href="#"><i class="fa fa-table"></i> <span> Blog</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                                            <ul>
                                                <li  ><a href="{{url('/add-blog')}}">Add blog</a></li>
                                                <li  ><a href="{{url('/view-blog')}}">View blog</a></li>

                                            </ul>
                                        </li>

                                        <li ><a href="#"><i class="fa fa-table"></i> <span> Jobs</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                                            <ul>
                                                <li  ><a href="{{url('/add-job')}}">Add job</a></li>
                                                <li  ><a href="{{url('/view-jobs')}}">View jobs</a></li>

                                            </ul>
                                        </li>

                                        @if(Auth::user()->role == "Admin")
        									<li><a href="{{url('/manage')}}"><i class="lnr lnr-pencil"></i> <span>Manage Alumni</span></a></li>
                                            <li><a href="{{url('/reports')}}"><i class="lnr lnr-pencil"></i> <span>Reports</span></a></li>
                                        @endif
								  </ul>
								</div>
                    @else

                    <div class="menu">
                        <ul id="menu" >
                            <li><a href="{{url('/login')}}"><i class="fa fa-tachometer"></i> <span>Login</span></a></li>
                            <li id="menu-academico" ><a href="{{url('/register')}}"><i class="fa fa-table"></i> <span> Register</span> </a></li>

                            </ul>
                        </div>

                    @endif

							  </div>
							  <div class="clearfix"></div>
							</div>
							<script>
							var toggle = true;

							$(".sidebar-icon").click(function() {
							  if (toggle)
							  {
								$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
								$("#menu span").css({"position":"absolute"});
							  }
							  else
							  {
								$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
								setTimeout(function() {
								  $("#menu span").css({"position":"relative"});
								}, 400);
							  }

											toggle = !toggle;
										});
							</script>
<!--js -->
<link rel="stylesheet" href="css/vroom.css">
<script type="text/javascript" src="js/vroom.js"></script>
<script type="text/javascript" src="js/TweenLite.min.js"></script>
<script type="text/javascript" src="js/CSSPlugin.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
</body>
</html>