@extends('layouts.app')

@section("content")
    <div class="container">



        <div class="panel panel-default col-md-10 ">
            @if( Session::has('success') )
                <div class="alert alert-success" style="margin-top: 10px;"  align="center">{{Session::get('success')}}</div>
            @endif

            @if( Session::has('error') )
                <div class="alert alert-success" style="margin-top: 10px" align="center">{{Session::get('error')}}</div>
            @endif
            <div class="panel-heading" >
                All News Items
            </div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Location</th>
                        <th>Details</th>
                        <th>Published On</th>
                        {{--<th></th>--}}
                    </tr>

                    @foreach($news as $item)
                        <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->location}}</td>
                            <td style="max-height:150px; overflow: scroll">{{$item->details}}</td>
                            <td>{{$item->created_at}}</td>
                            {{--<td>--}}
                                {{--@if(Auth::user()->role == "Admin")--}}
                                {{--<a href="{{url('/view-news/' . $item->nid)}}" class="btn btn-primary">View</a>--}}
                                {{--@endif--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>


    </div>
@endsection
