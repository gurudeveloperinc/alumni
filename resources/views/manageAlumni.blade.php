@extends('layouts.app')

@section('content')

<div class="container">
    <div class="panel panel-default col-md-8 col-md-offset-1">
        <div class="panel-heading">
            All Alumni <span style="float: right;" class="btn btn-success"><a style="color:white;" href="{{url('register')}}">Add Alumni</a> </span>
        </div>
        <div class="panel-body">
            <table class="table table-responsive">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Country</th>
                    <th>Registered On</th>
                </tr>

                @foreach($users as $item)
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->country}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>
                            <a href="{{url('/delete-user/' . $item->uid)}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

@endsection