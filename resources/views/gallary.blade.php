@extends('layouts.app')

@section("content")

    <div class="container">

        @foreach($gallary as $item)
        <div class="col-md-4">
            <img class="img-responsive" src="{{$item->url}}">
        </div>
        @endforeach
        @include('sidebar')
        @include('social')

    </div>
@endsection
