@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                 <div class="form-group">
                     <label for="bio" class="col-md-4 control-label">Bio</label>

                     <div class="col-md-6">
                            <textarea name="bio" rows="5" cols="35" >{{old('bio')}}</textarea>
                     </div>
                 </div>

                <div class="form-group">
                    <label for="society" class="col-md-4 control-label">Society</label>

                    <div class="col-md-6">
                        <input id="society" type="text" required class="form-control" name="society" value="{{ old('society') }}" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="studentid" class="col-md-4 control-label">Student ID</label>

                    <div class="col-md-6">
                        <input id="studentid" type="text" required class="form-control" name="studentid" value="{{ old('studentid') }}" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="programme" class="col-md-4 control-label">Programme</label>

                    <div class="col-md-6">
                        <input id="programme" type="text" required class="form-control" name="programme" value="{{ old('programme') }}" >
                    </div>
                </div>


                <div class="form-group">
                     <label for="country" class="col-md-4 control-label">Country</label>

                     <div class="col-md-6">
                         <input id="country" type="text" required class="form-control" name="country" value="{{ old('country') }}" >
                     </div>
                 </div>


                 <div class="form-group">
                     <label for="year" class="col-md-4 control-label">Year</label>

                     <div class="col-md-6">
                         <input id="year" type="text" required class="form-control" name="year" value="{{ old('year') }}" >
                     </div>
                 </div>


                 <div class="form-group">
                     <label for="phone" class="col-md-4 control-label">Phone</label>

                     <div class="col-md-6">
                         <input id="phone" pattern="[0-9]+" required type="text" class="form-control" name="phone" value="{{ old('phone') }}" >
                     </div>
                 </div>



                 <div class="form-group">
                     <label for="photo" class="col-md-4 control-label">Photo</label>

                     <div class="col-md-6">
                         <input id="photo" type="file" class="form-control" name="photo" value="{{ old('photo') }}" >
                     </div>
                 </div>

                  <div class="form-group">
                      <label for="cv" class="col-md-4 control-label">CV</label>

                      <div class="col-md-6">
                          <input id="cv" type="file" class="form-control" name="cv" value="{{ old('cv') }}" >
                      </div>
                  </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
