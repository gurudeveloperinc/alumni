@extends('layouts.app')
@section('content')
    <div class="flex-center position-ref full-height">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add Resource</div>
                        <div class="panel-body">
                            <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/add-resource') }}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="caption" class="col-md-4 control-label">Title</label>

                                    <div class="col-md-6">
                                        <input id="tittle" type="text" class="form-control" name="tittle" value="">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="url" class="col-md-4 control-label">Reporter</label>

                                    <div class="col-md-6">
                                        <input id="reporter" type="text" class="form-control" name="reporter">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="url" class="col-md-4 control-label">Comment</label>

                                    <div class="col-md-6">
                                        <input id="comment" type="text" class="form-control" name="comment">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="image" class="col-md-4 control-label">Image</label>

                                    <div class="col-md-6">
                                        <input id="image" type="file" class="form-control" name="image">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="url" class="col-md-4 control-label">File</label>

                                    <div class="col-md-6">
                                        <input id="url" type="file" class="form-control" name="url">

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
