@extends('layouts.app')

@section("content")

    <div class="container">
        <div class="panel panel-default col-md-8 col-md-offset-1">
            <div class="panel-heading">
                All Jobs
            </div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Details</th>
                        <th>Application Email</th>
                        <th>Expiry Date</th>
                        <th>Date</th>
                        <th></th>
                    </tr>

                    @foreach($jobs as $item)
                        <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->expiry}}</td>
                            <td>{{$item->created_at}}</td>

                            <td><a href="{{url('/apply/' . $item->jid)}}" class="btn btn-primary">Apply</a> </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection
