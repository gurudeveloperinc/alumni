@extends('layouts.app')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                @if( Session::has('success') )
                    <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
                @endif

                @if( Session::has('error') )
                    <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
                @endif

                    <div class="panel panel-default">
                    <div class="panel-heading">Add Blog</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/add-blog') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="">

                                </div>
                            </div>


                            <div class="form-group">
                                <label for="Details" class="col-md-4 control-label">Details</label>

                                <div class="col-md-6">
                                    <textarea id="details" class="form-control" name="details" ></textarea>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add
                                    </button>

                                    <button type="reset" class="btn btn-warning">
                                        Clear
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection