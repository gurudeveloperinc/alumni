@extends('layouts.new')

@section('content')

<!-- Slider-Starts-Here -->
<div class="slider" style="margin-top: 10px;">
    <ul class="rslides" id="slider">

        <li>
            <img src="images/r7.png" alt="10">
            <div class="caption">
                <!--<h3>Welcome to <span>SIET</span></h3>-->
                <!--<p>Teaching mastery in engineering</p>-->
            </div>
        </li>

        <li>
            <img src="images/r6.png" alt="10">
            <div class="caption">
                <!--<h3>Welcome to <span>SIET</span></h3>-->
                <!--<p>Teaching mastery in engineering</p>-->
            </div>
        </li>

        <li>
            <img src="images/r8.png" alt="10">
            <div class="caption">
                <!--<h3>Welcome to <span>SIET</span></h3>-->
                <!--<p>Teaching mastery in engineering</p>-->
            </div>
        </li>

        <li>
            <img src="images/r0.jpg" alt="10">
            <div class="caption">
                <!--<h3>Welcome to <span>SIET</span></h3>-->
                <!--<p>Teaching mastery in engineering</p>-->
            </div>
        </li>

        <li>
            <img src="images/r3.png" alt="Tradeaux">
            <div class="caption">
               <!-- <h3>Engineering</h3>-->
            </div>
        </li>
        <li>
            <img src="images/r4.png" alt="Tradeaux">
            <div class="caption">
              <!--  <h3>Informatics</h3>-->
            </div>
        </li>
        <li>
            <img src="images/r5.png" alt="Tradeaux">
            <div class="caption">
                <!--<h3>Technology</h3>-->
            </div>
        </li>

    </ul>
</div>
<!-- //Slider-Ends-Here -->

<!-- //Header-Ends-Here -->

<div class="content">
    <!-- Deans Message -Starts-Here -->
    <div class="area" id="deansMessage">

        <h1>Alumni Coordinator Message</h1>


        <div class="area1">

            <div class="col-md-12 col-sm-12 col-xs-12 air slideanim" style="background-color: white">

                <div class="col-md-6 col-sm-12" style="padding: 20px">
                    <img src="images/projectlogo.jpg" height="405px" alt="Tradeaux">
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="area-info">
                        <h4>MR DANIEL OKU TETTEH</h4>
                        <p>Welcome to the alumni portal of Regent University College of Science and Technology (RUCST), a place where lives are shaped, and destinies are formed to impact generations. We are committed to raising visionary, ethical and dedicated passionate leaders to contribute positively to the African renaissance.</p>
                    </div>
                </div>

            </div>


            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //Deans Message-Ends-Here -->


    <!-- HOD's Message-Starts-Here -->
    <div class="area" id="hod">

        <h1>FEATURED ALUMNI</h1>

        <div class="area1">
            <div class="col-md-4 col-sm-4 air slideanim">
                <div class="area-image">
                    <img src="images/alu.png" >
                </div>
                <div class="area-info">
                    <h4>Rudolf  Akrong</h4>
                    <p>Samuel Chris Quist is the HOD of the Informatics department. He is a Computer Scientist with passion for Database Systems and System Development.</p>
                    <a href="{{url('/featured/1')}}" class="btn btn-success">View Profile</a>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 sea slideanim">
                <div class="area-image">
                    <img src="images/alu1.png" alt="Tradeaux">
                </div>
                <div class="area-info">
                    <h4>Kweku Otchere </h4>
                    <p>Lead Programmer, Script House Ltd</p>
                    <a href="{{url('/featured/2')}}" class="btn btn-success">View Profile</a>

                </div>
            </div>

            <div class="col-md-4 col-sm-4 land slideanim">
                <div class="area-image">
                    <img src="images/alu3.png" alt="Tradeaux">
                </div>
                <div class="area-info">
                    <h4>Kwame Aware </h4>
                    <p>Service Engineer, Mantrac Ghana Limited.</p>
                    <a href="{{url('/featured/3')}}" class="btn btn-success">View Profile</a>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 viewCourses">
            <p>
                To view alumni by year <a href="{{url('/view-alumni/All')}}" class="btn btn-success">Click here</a>
            </p>
        </div>

    </div>
    <!-- //HOD's-Ends-Here -->

    <!-- news and updates -->

    <div class="row news" align="center">
        <h3>Recent news</h3>
        @foreach($news as $item)
            <div class="col-md-3">
                <img class="img-responsive" src="images/logo.png" style="height: 180px;">
                <h4>{{$item->title}}</h4>
                <p style=" height: 200px !important;
                overflow: hidden;">{{$item->details}}
                </p>
            </div>

        @endforeach

        <div class="col-md-12 viewCourses">
            <p>
                To view all news <a href="{{url('/view-news')}}" class="btn btn-success">Click here</a>
            </p>
        </div>

    </div>

    @if(!Auth::guest())
    <!-- HOD's Message-Starts-Here -->
    <div class="area resources" id="hod">

        <h1>RESOURCES</h1>

        <div class="area1">

                @foreach($resources as $item)
                <div class="col-md-4 col-sm-4 air slideanim">

                <div class="area-image">
                    <img src="{{$item->image}}" >
                </div>
                <div class="area-info">
                    <h4>{{$item->title}} </h4>
                    <span>Uploaded by {{$item->reporter}}</span>
                    <a href="{{$item->url}}" class="btn btn-success">DOWNLOAD</a>
                </div>
            </div>

                @endforeach


            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 viewCourses">
            <p>
                To view all resources <a href="{{url('/view-resources')}}" class="btn btn-success">Click here</a>
            </p>
        </div>

    </div>
    <!-- //HOD's-Ends-Here -->

    <!-- Events-Starts-Here -->
    <div class="area events" id="hod">

        <h1>EVENTS</h1>

        <div class="area1">
            <div class="col-md-4 col-sm-4 air slideanim">
           @foreach($events as $item)
                <div class="area-info">
                    <h4>{{$item->evtittle}}</h4>
                    <p>{{$item->evdetails}}</p>
                    <span class="well well-sm">{{$item->evdate}} | {{$item->venue}}</span>
                </div>
               @endforeach
            </div>


            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 viewCourses">
            <p>
                To view all events <a href="{{url('/view-events')}}" class="btn btn-success">Click here</a>
            </p>
        </div>

    </div>
    <!-- //Events-Ends-Here -->

    @endif


    <!-- Contact-Starts-Here -->
    <div class="contact slideanim" id="contact">
        <h3>Contact</h3>
        <form class="contact_form">

            <div class="message">
                <div class="col-md-6 col-sm-6 grid_6 c1">
                    <input type="text" class="text" value="Name" placeholder="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
                    <input type="text" class="text" value="Email" placeholder="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
                    <input type="text" class="text" value="Phone" placeholder="Phone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone';}">
                </div>

                <div class="col-md-6 col-sm-6 grid_6 c1">
                    <textarea placeholder="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                </div>
                <div class="clearfix"> </div>
            </div>

            <input type="submit" class="more_btn" value="Send Message">
        </form>
    </div>
    <!-- //Contact-Ends-Here -->

</div>

<!-- Map-iFrame -->
<div class="map slideanim">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2361.197352958536!2d-0.2985475012367058!3d5.560381689555123!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x100027704b3613a4!2sRegent+University!5e0!3m2!1sen!2sgh!4v1459871289162" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- //Map-iFrame -->


@endsection