@extends('layouts.app')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                @if( Session::has('success') )
                    <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
                @endif

                @if( Session::has('error') )
                    <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
                @endif

                    <div class="panel panel-default">
                    <div class="panel-heading">Add Job</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/add-job') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="">

                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-md-4 control-label">Application email</label>

                                <div class="col-md-6">
                                    <input  type="email" class="form-control" name="email" value="">

                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-md-4 control-label">Expiry Date</label>

                                <div class="col-md-6">
                                    <input  type="text" pattern="[0-9+][0-9+]-[0-9+][0-9+]-[0-9+][0-9+]" placeholder="Eg. 12-10-17" aria-invalid="Please validate" class="form-control validate" name="expiry" value="">

                                </div>
                            </div>


                            <div class="form-group">
                                <label for="Details" class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea id="details" rows="10" class="form-control" name="description" ></textarea>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add
                                    </button>

                                    <button type="reset" class="btn btn-warning">
                                        Clear
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection