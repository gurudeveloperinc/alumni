@extends('layouts.app')

@section("content")

    <div class="container">
        <div class="panel panel-default col-md-8 col-md-offset-1">
            <div class="panel-heading">
                All Events
            </div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Details</th>
                        <th>Venue</th>
                        <th>Created On</th>
                        <th></th>
                    </tr>

                    @foreach($events as $item)
                        <tr>
                            <td>{{$item->evtittle}}</td>
                            <td>{{$item->evdate}} {{$item->evyear}}</td>
                            <td>{{$item->evdetails}}</td>
                            <td>{{$item->venue}}</td>
                            <td>{{$item->created_at}}</td>
                            @if(!Auth::guest())
                                @if(Auth::user()->role == "Admin")
                                <td><a href="{{url('/delete-event/' . $item->evid)}}" class="btn btn-danger">Delete</a></td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection
