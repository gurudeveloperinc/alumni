@extends('layouts.app')
@section('content')
<div>

     class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif




        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if(Session::has('success'))

                    <div align="center" class="alert alert-success">{{Session::get('success')}}</div>
                @endif

                @if(Session::has('error'))

                    <div align="center" class="alert alert-danger">{{Session::get('error')}}</div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Gallery</div>
                    <div class="panel-body">
                        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/create-gallery') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="caption" class="col-md-4 control-label">Caption</label>

                                <div class="col-md-6">
                                    <input id="caption" type="text" class="form-control" name="caption" value="">

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="url" class="col-md-4 control-label">Url</label>

                                <div class="col-md-6">
                                    <input id="url" type="file" class="form-control" name="url">

                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        CreateGallery
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
