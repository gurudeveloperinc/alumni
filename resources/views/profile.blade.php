@extends('layouts.app')
@section('content')
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-user-md"></i> Profile</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="{{url('/home')}}">Home</a></li>
						<li><i class="fa fa-user-md"></i>Profile</li>
					</ol>
				</div>
			</div>
              <div class="row">
                <!-- profile-widget -->
                <div class="col-lg-12">
                    <div class="profile-widget profile-widget-info">
                          <div class="panel-body">
                            <div class="col-lg-6 col-sm-6">
                              <h4>{{Auth::user()->name}}</h4>
                              <div class="follow-ava">
                                  <img src="img/user1.png" alt="">
                              </div>
                              <h6>{{Auth::user()->role}}</h6>
                            </div>
                            <div class="col-lg-4 col-sm-6 follow-info">
                                <p>{{Auth::user()->bio}}</p>
                                <p></p>
								<p><i class="fa fa-twitter">{{Auth::user()->name}}</i></p>
                                <h6>
                                    <span><i class="icon_clock_alt"></i>{{Carbon\Carbon::now()->toFormattedDateString()}}</span>

                                </h6>
                            </div>

                          </div>
                    </div>
                </div>
              </div>
              <!-- page start-->
              <div class="row">
                 <div class="col-lg-12">
                    <section class="panel">
                          <header class="panel-heading tab-bg-info">
                              <ul class="nav nav-tabs">
                                  <li class="active">
                                      <a data-toggle="tab"  href="#profile">
                                          <i class="icon-user"></i>
                                          Profile
                                      </a>
                                  </li>
                                  <li class="">
                                      <a data-toggle="tab" href="#edit-profile">
                                          <i class="icon-envelope"></i>
                                          Edit Profile
                                      </a>
                                  </li>
                              </ul>
                          </header>
                          <div class="panel-body">
                              <div class="tab-content">

                                  <!-- profile -->
                                  <div id="profile" class="active tab-pane">
                                    <section class="panel">
                                      <div class="bio-graph-heading panel-heading">

                                      </div>
                                      <div class="panel-body bio-graph-info">
                                         <h3>Biography</h3>
                                          <div class="row">
                                              <div class="bio-row">
                                                  <p><span>First Name </span>: {{Auth::user()->name}} </p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Last Name </span>: Tetteh</p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Birthday</span>: 27 August 1987</p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Country </span>: {{Auth::user()->country}}</p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Occupation </span>: {{Auth::user()->bio}}</p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Email </span>: {{Auth::user()->email}}</p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Mobile </span>: {{Auth::user()->phone}}</p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Student ID </span>: {{Auth::user()->studentid}}</p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Programme </span>: {{Auth::user()->programme}}</p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Society </span>: {{Auth::user()->society}}</p>
                                              </div>
                                          </div>
                                      </div>
                                    </section>
                                      <section>
                                          <div class="row">
                                          </div>
                                      </section>
                                  </div>
                                  <!-- edit-profile -->
                                  <div id="edit-profile" class="tab-pane">
                                    <section class="panel">
                                          <div class="panel-body bio-graph-info">
                                              <h1> Profile Info</h1>
                                              <form class="form-horizontal" role="form">
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">First Name</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" id="f-name" placeholder=" ">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Last Name</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" id="l-name" placeholder=" ">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">About Me</label>
                                                      <div class="col-lg-10">
                                                          <textarea name="" id="" class="form-control" cols="30" rows="5"></textarea>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Country</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" id="c-name" placeholder=" ">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Birthday</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" id="b-day" placeholder=" ">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Occupation</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" id="occupation" placeholder=" ">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Email</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" id="email" placeholder=" ">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Mobile</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" id="mobile" placeholder=" ">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Website URL</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" id="url" placeholder="http://www.demowebsite.com ">
                                                      </div>
                                                  </div>

                                                  <div class="form-group">
                                                      <div class="col-lg-offset-2 col-lg-10">
                                                          <button type="submit" class="btn btn-primary">Save</button>
                                                          <button type="button" class="btn btn-danger">Cancel</button>
                                                      </div>
                                                  </div>
                                              </form>
                                          </div>
                                      </section>
                                  </div>
                              </div>
                          </div>
                      </section>
                 </div>
              </div>

              <!-- page end-->
          </section>
      </section>
@endsection
